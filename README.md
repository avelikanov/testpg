# UCLA Web Accessibility Initiative Wordpress Plugin
Contributors:      UCLA Web Accessibility Initiative Team, [UCLA OIT Disabilities and Computing Program](https://dcp.ucla.edu/), [UCLA Strategic Communications](https://strategic-communications.ucla.edu/)

Tags:              block

Requires at least: 5.3.2

Tested up to:      5.4.1

Stable tag:        0.1.0

Requires PHP:      7.0.0

UWAI&#39;s Blocks for wordpress.

## Description

This plugin contains a varaity of UCLA branded, accessible blocks for the Gutenberg editor. 

## Included Blocks
### Finished/Stable
- Tile Link Card
- Styled Buttons
### WIP Blocks (Unstable and likely to break without notice)
- Profile Card

For backwards compatibility, if this section is missing, the full length of the short description will be used, and
Markdown parsed.

## Installation

This section describes how to install the plugin and get it working.

1. Upload the plugin files to the `/wp-content/plugins/wp-uwai-extras` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the plugin by editing any page in the Gutenberg editor


## Frequently Asked Questions

### Is this a standalone plugin or does it go along with a theme?

Well, I'm glad you asked that rather specific question. This plugin was designed in concert with the [UCLA Branded Accessible WordPress Theme](https://bitbucket.org/uclaucomm/ucla-sc), and, although it works/fits best when used with that theme, the plugin should work independently as well.

## License

## Changelog

### 0.1.0
* Initial Release
